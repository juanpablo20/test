# Best Effort Bot

Best Effort Bot is a security policy password detection system, which is based on detecting the minimum security policy on internet domains in order to generate a successful registration. 

## Getting started 🚀

clone the repository on the local machine using the following command 

$ git clone https://gitlab.com/dvizcaya91/best-effort-bot.git

Or using the clone button of the repository 

### Prerequisites

before starting, it is recommended that your virtual machine has the following libraries installed to ensure a correct functionality:

```
blinker==1.4
certifi==2021.5.30
cffi==1.14.6
charset-normalizer==2.0.3
click==8.0.1
cryptography==3.4.7
docopt==0.6.2
emoji==1.4.1
Faker==8.10.2
h11==0.12.0
h2==4.0.0
hpack==4.0.0
hyperframe==6.0.1
idna==3.2
importlib-metadata==4.6.1
joblib==1.0.1
kaitaistruct==0.9
lxml==4.6.3
nltk==3.6.2
numpy==1.21.1
pipreqs==0.4.10
psutil==5.8.0
pyasn1==0.4.8
pycparser==2.20
pydantic==1.8.2
pyOpenSSL==20.0.1
pyparsing==2.4.7
PySocks==1.7.1
pysqlite3==0.4.6
python-dateutil==2.8.2
pytz==2021.1
regex==2021.7.6
requests==2.26.0
scikit-learn==0.23.0
scipy==1.7.0
selenium==3.141.0
selenium-wire==4.4.0
six==1.16.0
text-unidecode==1.3
threadpoolctl==2.2.0
tqdm==4.61.2
typing-extensions==3.10.0.0
urllib3==1.26.6
wsproto==1.0.0
yarg==0.1.9
zipp==3.5.0
```

to simplify the installation process, it is recommended to use the requirements.txt file and the following command to install all the requirements

```
$ pip install -r requirements.txt
```
### Google chrome 

To run the software it is necessary to download google chrome and the corresponding driver version. the driver version can be obtained from https://chromedriver.chromium.org/downloads.

The Chrome driver should be placed in the path signup_code/assets from the repository directory

### OS support 

Best Effort Bot is stable with linux 

## Configuration 🔧

To configure the system go to the following directory path: signup_code/run_config.py and edit it with the necessary parameters

### Folders 

1) Create an output folder to save csv output data
2) Create a folder where screenshots will be stored.(only if it is neccesary)

### Optionals

1) Create a S3 bucket to save the data 


### Parameters
```
SOURCE_FILE (str) --> csv file with all domains to be analyzed

PROCESSES_TO_EVAL (int) --> the number of processes that will run in parallel according to the number of cores of the machine. Leave empty to automatically set processes to total cores number

OUTPUT_FOLDER (str) --> Path to output folder for csv files 

RESUME_DB (str) --> Name of resume DB file

THREAD_PADDING_INTERVAL (int) --> The wait interval between starting consecutive analysis threads.

DOMAIN_BATCH_SIZE (int) --> Set to -1 to automatically set batch size based on number of processes

MIN_BATCH_SIZE (int) --> Minimun domain batch size

CHROME_MAX_TIME (int) --> Maximum time a chrome process will remain open in seconds

TAKE_SCREENSHOTS (bool) --> use if you want to take screenshots before, during and after the registration process 

SAVE_MODEL_INPUT (bool) --> (Do not modify) stores the inputs data of the machine learning model (Random Forest)

FAKE_EMAILS (bool) --> (Set True only for testing) generates fake e-mails for domain signup page

DB_FILE (str) --> Name of db files that will store singup stages

DB_TIMEOUT (int) --> Timeout for signup stages db in ms

DEBUG_FOLDER (str) --> Folder where screenshots will be stored.

FORM_FILLING_WAITTIME (int) --> is the waiting time in seconds while the landignpage is loading. 

ROOT (str) --> String that will be added at the end of each username and email(before @)

SERVER_ID (str) --> is an unique ID for each AWS instance

BUKET (str) --> (OPTIONAL) is the name of the S3 bucket where the data will be stored

UPLOAD_TO_S3 (bool) --> Decides wheter upload files to amazon S3 or not

IS_MAIN_SERVER (bool) --> This parameter defines if the current server will be in charge of merge all S3 files. It's important to set it to True in only one server

TRUE_POSITIVES_ATTEMPT_THRESHOLD (int) --> The number of attempts from which is not normal to have only false results, it is recommended to set the value at 10
```
#### Parameters Current Example

```
SOURCE_FILE = 'instance_0_final_run.csv'
PROCESSES_TO_EVAL = [] #Leave empty to automatically set processes to total cores number
OUTPUT_FOLDER = 'final_run_results' #Path to output folder for csv files
RESUME_DB = 'completed_domains.sqlite' #Name of resume DB file
#WAIT_TIME = 3 #Domains wait time
THREAD_PADDING_INTERVAL = 102 # The wait interval between starting consecutive analysis threads.
DOMAIN_BATCH_SIZE = 126 #Set to -1 to automatically set batch size based on number of processes
MIN_BATCH_SIZE = 0 #Minimun domain batch size
CHROME_MAX_TIME = 75*60 #Maximum time a chrome process will remain open in seconds
TAKE_SCREENSHOTS = False
SAVE_MODEL_INPUT = False
FAKE_EMAILS = False #Set True only for testing
DB_FILE = 'signup.sqlite' #Name of db files that will store singup stages
DB_TIMEOUT = 30*60*1000#ms #Timeout for signup stages db
DEBUG_FOLDER = 'debug_results' #Folder where screenshots will be stored.
FORM_FILLING_WAITTIME = 5 #s
ROOT = 'befr' #String that will be added at the end of each username and email(before @)
SERVER_ID = 's1' #Unique ID for each AWS instance
BUKET = 'best-effort-final-run' #S3 bucket name
UPLOAD_TO_S3 = True #Decides wheter upload files to amazon S3 or not
IS_MAIN_SERVER = False #This parameter defines if the current server will be in charge of merge all S3 files. It's important to set it to True in only one server
TRUE_POSITIVES_ATTEMPT_THRESHOLD = 10 #The number of attempts from which is not normal to have only false results```
```
## Deployment 📦

To run Best Effort Bot:

- Go to the BestEffort local path 
- Execute the following command: 

```
Python3 parallelization_test.py
```

if an error occurs with the local machine and you want to rerun the process and resume it, follow the steps below in the shell console:

```
pkill python3
pkill chrome
Python3 parallelization_test.py -r
```

if the local machine freeze perform the following steps:

- Re-start the local machine
- Go to the BestEffort local path 
- Execute the following command to resume the run: 

```
Python3 parallelization_test.py -r
```

## Built With 🛠️

- blinker
- certifi
- cffi
- charset-normalizer
- click
- cryptography
- docopt
- emoji
- Faker
- h11
- h2
- hpack
- hyperframe
- idna
- importlib-metadata
- joblib
- kaitaistruct
- lxml
- nltk
- numpy
- pipreqs
- psutil
- pyasn1
- pycparser
- pydantic
- pyOpenSSL
- pyparsing
- PySocks
- pysqlite3
- python-dateutil
- pytz
- regex
- requests
- scikit-learn
- scipy
- selenium
- selenium-wire
- six
- text-unidecode
- threadpoolctl
- tqdm
- typing-extensions
- urllib3
- wsproto
- yarg
- zipp

## Versioning 📌

We use [gitlab](http://gitlab.com/) for versioning. 

## Authors ✒️


## License 📄


## Acknowledgments

